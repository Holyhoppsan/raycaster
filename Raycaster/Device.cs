﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Raycaster
{
    public class Device
    {
        private byte[] mBackBuffer;
        private WriteableBitmap mBitmap;
        private static int mCurrentFrameCount;
        private static int mLastFrameCount;
        private static int mLastTick;

        private const int COLORDEPTH = 4;

        public int GetFrameRate()
        {
            return mLastFrameCount;
        }

        public double SurfaceWidth {
            get
            {
                return mBitmap.Width;
            }
        }

        public double SurfaceHeight
        {
            get
            {
                return mBitmap.Height;
            }
        }

        public Device(WriteableBitmap bitmap)
        {
            this.mBitmap = bitmap;
            mCurrentFrameCount = 0;
            mLastFrameCount = 0;
            mLastTick = System.Environment.TickCount;

            mBackBuffer = new byte[mBitmap.PixelWidth * mBitmap.PixelHeight * COLORDEPTH];
        }

        public void Clear(byte r, byte g, byte b, byte a)
        {
            for(var index = 0; index < mBackBuffer.Length; index+= 4)
            {
                mBackBuffer[index] = b;
                mBackBuffer[index + 1] = g;
                mBackBuffer[index + 2] = r;
                mBackBuffer[index + 3] = a;
            }
        }

        public void PlotPixel(int x, int y, byte r, byte g, byte b, byte a = 255)
        {
            int arrayIndex = (x + (y * (int)mBitmap.Width)) * COLORDEPTH;

            mBackBuffer[arrayIndex] = b;
            mBackBuffer[arrayIndex + 1] = g;
            mBackBuffer[arrayIndex + 2] = r;
            mBackBuffer[arrayIndex + 3] = a;
        }

        public void Present()
        {
            var rect = new System.Windows.Int32Rect(0, 0, 640, 480);
            mBitmap.WritePixels(rect, mBackBuffer, COLORDEPTH * (int)mBitmap.Width, 0);

            if (System.Environment.TickCount - mLastTick >= 1000)
            {
                mLastFrameCount = mCurrentFrameCount;
                mCurrentFrameCount = 0;
                mLastTick = System.Environment.TickCount;
            }

            mCurrentFrameCount++;
        }
    }
}
