﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Raycaster
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Device device;
        private RaycastView view;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var bitmap = new WriteableBitmap(640, 480, 96.0,96.0, PixelFormats.Bgra32, null);

            device = new Device(bitmap);
            view = new RaycastView(device);

            frontBuffer.Source = bitmap;

            CompositionTarget.Rendering += CompositionTarget_Rendering;
        }

        void CompositionTarget_Rendering(object sender, object e)
        {
            device.Clear(135, 206, 250, 255);

            view.Render(new Level(), new Player());

            device.Present();

            frameCounter.Content = device.GetFrameRate();
        }
    }
}
