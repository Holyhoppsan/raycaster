﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raycaster
{
    public class Player
    {
        public double PosX { get; set; }
        public double PosY { get; set; }

        public double DirX { get; set; }
        public double DirY { get; set; }

        public Player()
        {
            PosX = 22;
            PosY = 12;
            DirX = -1;
            DirY = 0;
        }
    }
}
