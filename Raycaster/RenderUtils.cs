﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raycaster
{
    public class RenderUtils
    {
        public static void DrawVerticalLine(Device device, int x1, int y1, int x2, int y2, byte r, byte g, byte b, byte a = 255)
        {
            if(x1 != x2)
            {
                throw new ArgumentException("The x parameters are not equal.");
            }

            for(int y = y1; y < y2; y++)
            {
                device.PlotPixel(x1, y, r, b, g, a);
            }
        }
    }
}
