﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raycaster
{
    public class RaycastView
    {
        private Device Device { get; set; }

        public RaycastView(Device device)
        {
            Device = device;
        }

        public void Render(Level level, Player player)
        {
            for(int x = 0; x < (int)Device.SurfaceWidth; x++)
            {
                RenderUtils.DrawVerticalLine(Device, x, 150, x, 400, 255, 0, 0);
            }
        }

    }
}
